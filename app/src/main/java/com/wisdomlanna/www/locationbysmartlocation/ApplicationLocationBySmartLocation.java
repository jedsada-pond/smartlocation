package com.wisdomlanna.www.locationbysmartlocation;

import android.app.Application;

/**
 * Created by jedsada on 23/4/2559.
 */
public class ApplicationLocationBySmartLocation extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Contextor.getInstance().init(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
