package com.wisdomlanna.www.locationbysmartlocation;

import android.content.Context;
import android.location.Location;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationAccuracy;
import io.nlopez.smartlocation.location.config.LocationParams;

/**
 * Created by jedsada on 23/4/2559.
 */
public class LocationManager {
    private static LocationManager instance;
    private final SmartLocation smartLocation;
    private final LocationParams params;
    private Context context;
    private Location location;
    private final int interval = 2000;

    public static LocationManager getInstance() {
        if (instance == null)
            instance = new LocationManager();

        return instance;
    }

    private LocationManager() {
        context = Contextor.getInstance().getContext();

        params = new LocationParams.Builder()
                .setAccuracy(LocationAccuracy.HIGH)
                .setInterval(interval)
                .build();

        smartLocation = new SmartLocation.Builder(context)
                .logging(true)
                .build();
    }

    OnLocationUpdatedListener locationUpdatedListener = new OnLocationUpdatedListener() {
        @Override
        public void onLocationUpdated(Location location) {
            setLocation(location);
        }
    };

    public void startLOcation() {
        smartLocation.location().config(params).start(locationUpdatedListener);
    }

    public void stopLocation() {
        smartLocation.location().stop();
    }

    public boolean locationServicesEnabled() {
        return smartLocation.location().state().locationServicesEnabled();
    }

    public boolean isAnyProviderAvailable() {
        return smartLocation.location().state().isAnyProviderAvailable();
    }


    public boolean isGpsAvailable() {
        return smartLocation.location().state().isGpsAvailable();
    }

    public boolean isNetworkAvailable() {
        return smartLocation.location().state().isNetworkAvailable();
    }

    public boolean isPassiveAvailable() {
        return smartLocation.location().state().isPassiveAvailable();
    }


    public boolean isMockSettingEnabled() {
        return smartLocation.location().state().isMockSettingEnabled();
    }

    public Location getLocation() {
        return location;
    }

    private void setLocation(Location location) {
        this.location = location;
    }
}
