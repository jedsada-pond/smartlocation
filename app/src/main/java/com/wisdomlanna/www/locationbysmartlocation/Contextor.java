package com.wisdomlanna.www.locationbysmartlocation;

import android.content.Context;

/**
 * Created by jedsada on 23/4/2559.
 */
public class Contextor {
    private static Contextor instance;
    private static Context context;

    public static Contextor getInstance(){
        if(instance == null)
            instance = new Contextor();
        return instance;
    }

    public Contextor() {
    }

    public void init(Context context){
        this.context = context;
    }

    public Context getContext(){
        return context;
    }
}
